package com.daffodil.assignment2;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.SeriesSelection;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.java_websocket.client.WebSocketClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private WebSocketClient socketClient;
    private int id;
    private String name;
    private double value;
    //    private double four;
//    private TextView textView;
    private XYSeries chart1series;
    private GraphicalView chart1chartView;
    private XYMultipleSeriesRenderer chart1mRenderer;
    private XYMultipleSeriesDataset chart1dataset;
    private LinearLayout chart1chartLyt;
    private int niftyValueIndex = 0;
    private Thread niftyAnnotationClose = null;
    private XYSeries chart2series2;
    private GraphicalView chart2chartView2;
    private XYMultipleSeriesRenderer chart2mRenderer2;
    private XYMultipleSeriesDataset chart2dataset2;
    private LinearLayout chart2chartLyt2;
    private int sensaxValueIndex = 0;
    private Thread sensaxAnnotationClose = null;
    private double niftyValues[] = new double[61000];
    private long niftyTimes[] = new long[61000];
    private long timeNiftyFirstVal = 0;
    private long niftyDiffTime = 0;
    private double sensaxValues[] = new double[61000];
    private long sensaxTimes[] = new long[61000];
    private long timeSensaxFirstVal = 0;
    private long sensaxDiffTime = 0;
    private Button startButton;
    private Button stopButton;
    private SocketManager socketManager;
    private RecvMsgList RMLImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        textView = (TextView) findViewById(R.id.editText);
        startButton = (Button) findViewById(R.id.button1);
        stopButton = (Button) findViewById(R.id.button2);

        socketManager = SocketManager.getInstance();
        socketClient = socketManager.getStockSocket();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // startPlottingChart();
    }

    @Override
    protected void onResume() {
        startButton.setClickable(true);
        stopButton.setClickable(false);
        super.onResume();
        // startPlottingChart();
    }

    private void startPlottingChart() {
        initChart1();
        initChart2();

//        final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);

        RMLImpl = new RecvMsgList() {
            @Override
            public void onReceived(final String receivedMessage) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // textView.setText(/*textView.getText().toString() + "\n" +*/ msg);
                        // scrollView.fullScroll(View.FOCUS_DOWN);

                        try {
                            JSONObject obj = new JSONObject(receivedMessage);
                            id = obj.optInt(AppConst.JSON_KEY_ID);
                            name = obj.optString(AppConst.JSON_KEY_NAME);
                            value = obj.optDouble(AppConst.JSON_KEY_VALUE);
//                            four = obj.optDouble(AppConst.JSON_KEY_FOUR);
                            if (id == AppConst.SENSEX_NIFTY_DATA_ID /* || id==12 || id == 5*//* earlier it was 11 */) {
                                if (name.equals(AppConst.SENSAX_NAME)) {
                                    // sensax
                                    // old val
                                    // updateChart2(value);
                                    startUpdatingChart(value, AppConst.CHART2);
                                    // Log.d("sensax","sensax input "+value);
                                } else if (name.equals(AppConst.NIFTY_NAME)) {
                                    // nifty
                                    // old val
                                    // updateChart1(value);
                                    startUpdatingChart(value, AppConst.CHART1);
                                    // Log.d("nifty","nifty input "+value);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }
        };
        socketManager.recvMsgList = RMLImpl;

        // new RandomDataSend(list).start();
    }

    private void initRenderer(XYSeriesRenderer renderer) {
        renderer.setAnnotationsColor(Color.BLACK);
        renderer.setAnnotationsTextSize(getSP(AppConst.SMALL_TEXT_SIZE));    // 15

        renderer.setLineWidth(2);
        renderer.setColor(Color.RED);
        // Include low and max value
        renderer.setDisplayBoundingPoints(true);
        // we add point markers
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setPointStrokeWidth(3);
    }

    private void initMRenderer(XYMultipleSeriesRenderer mRenderer) {

        // We want to avoid black border
        mRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins


        // Disable Pan on two axis
        mRenderer.setPanEnabled(false, false);
        mRenderer.setYAxisMax(10);
        mRenderer.setYAxisMin(0);
        mRenderer.setXAxisMin(0);
        mRenderer.setXAxisMax(61);
        mRenderer.setShowGrid(true); // we show the grid
        int[] margin = {0, 100, 0, 0};
        mRenderer.setMargins(margin);

        mRenderer.setXTitle(getResources().getString(R.string.x_title));
        mRenderer.setYTitle(getResources().getString(R.string.y_title));

        mRenderer.setAxisTitleTextSize(20);
        mRenderer.setLabelsColor(Color.BLACK);

        mRenderer.setXAxisColor(Color.BLACK);
        mRenderer.setYAxisColor(Color.BLACK);


        mRenderer.setYLabelsAlign(Paint.Align.RIGHT);

        mRenderer.setYLabelsColor(0, Color.BLACK);
        mRenderer.setXLabelsColor(Color.BLACK);

        mRenderer.setXLabels(10);
        mRenderer.setYLabels(10);

        mRenderer.setAxisTitleTextSize(getSP(AppConst.MEDIUM_TEXT_SIZE));

        mRenderer.setLabelsTextSize(getSP(AppConst.SMALL_TEXT_SIZE));

        mRenderer.setPanEnabled(false, false);
        mRenderer.setZoomEnabled(false, false);

        mRenderer.setXLabelsPadding(10);
        mRenderer.setYLabelsPadding(10);
    }

    private void initChart1() {
        chart1series = new XYSeries(getResources().getString(R.string.nifty_legend));
        chart1dataset = new XYMultipleSeriesDataset();
        chart1dataset.addSeries(chart1series);

        // Now we create the renderer
        final XYSeriesRenderer renderer = new XYSeriesRenderer();
        initRenderer(renderer);

        chart1mRenderer = new XYMultipleSeriesRenderer();
        chart1mRenderer.addSeriesRenderer(renderer);
        initMRenderer(chart1mRenderer);

        chart1chartView = ChartFactory.getLineChartView(this, chart1dataset, chart1mRenderer);
        chart1chartView.setOnClickListener(this);
        chart1chartLyt = (LinearLayout) findViewById(R.id.chart1);
        chart1chartLyt.addView(chart1chartView, 0);
    }

    private void initChart2() {
        chart2series2 = new XYSeries(getResources().getString(R.string.sensax_legend));
        chart2dataset2 = new XYMultipleSeriesDataset();
        chart2dataset2.addSeries(chart2series2);

        // Now we create the renderer
        final XYSeriesRenderer renderer2 = new XYSeriesRenderer();
        initRenderer(renderer2);

        chart2mRenderer2 = new XYMultipleSeriesRenderer();
        chart2mRenderer2.addSeriesRenderer(renderer2);
        initMRenderer(chart2mRenderer2);

        chart2chartView2 = ChartFactory.getLineChartView(this, chart2dataset2, chart2mRenderer2);
        chart2chartView2.setOnClickListener(this);

        chart2chartLyt2 = (LinearLayout) findViewById(R.id.chart2);
        chart2chartLyt2.addView(chart2chartView2, 0);

    }

    private void clearAnnotation(double x, double y, XYMultipleSeriesRenderer mRenderer) {
        if (mRenderer.equals(chart1mRenderer)) {
            if (chart1series.getAnnotationCount() != 0) {
                chart1series.removeAnnotation(0);
            }
            chart1mRenderer.removeXTextLabel(x);
            chart1mRenderer.removeYTextLabel(y);
        } else if (mRenderer.equals(chart2mRenderer2)) {
            if (chart2series2.getAnnotationCount() != 0) {
                chart2series2.removeAnnotation(0);
            }
            chart2mRenderer2.removeXTextLabel(x);
            chart2mRenderer2.removeYTextLabel(y);
        }

    }

    long orig;

    private void startUpdatingChart(double val, boolean chartType) {
        XYSeries series = null;
        //XYMultipleSeriesDataset dataset = null;
        XYMultipleSeriesRenderer mRenderer = null;
        GraphicalView chartView = null;
        orig = 0;

        if (chartType == AppConst.CHART1) {
            series = chart1series;
            mRenderer = chart1mRenderer;
            chartView = chart1chartView;
            orig = timeNiftyFirstVal;
        } else if (chartType == AppConst.CHART2) {
            series = chart2series2;
            mRenderer = chart2mRenderer2;
            chartView = chart2chartView2;
            orig = timeSensaxFirstVal;
        }

        long x = update(val, series, mRenderer, chartView, orig);
        if (chartType == AppConst.CHART1) {
            timeNiftyFirstVal = x;
        } else {
            timeSensaxFirstVal = x;
        }
    }

    private long update(double val, XYSeries series, XYMultipleSeriesRenderer mRenderer, GraphicalView chartView, long origTime) {
        long orig = origTime;

        int index = series.getItemCount();
        long timeDiff = 0;
        double min, max;
        boolean flag = false;
        ArrayList<Double> value = new ArrayList<>();
        ArrayList<Double> time = new ArrayList<>();


        if (index == 0) {
            orig = System.currentTimeMillis();
            mRenderer.setYAxisMin(val - 10);
            mRenderer.setYAxisMax(val + 10);
        } else {
            timeDiff = System.currentTimeMillis() - orig;
        }

        min = mRenderer.getYAxisMin();
        max = mRenderer.getYAxisMax();
        timeDiff = timeDiff / 1000;

        if (timeDiff > 60) {
            orig = System.currentTimeMillis();

            for (int i = 0; i < index; i++) {
                series.remove(0);
            }
            mRenderer.setYAxisMin(val - 10);
            mRenderer.setYAxisMax(val + 10);
        }

        if (val > max) {
            mRenderer.setYAxisMax(val);
            flag = true;
        } else if (val < min) {
            mRenderer.setYAxisMin(val);
            flag = true;
        }

        if (flag) {
            for (int i = 0; i < index; i++) {
                value.add(series.getY(0));
                time.add(series.getX(0));
                series.remove(0);
            }
            for (int i = 0; i < index; i++) {
                series.add(time.get(i), value.get(i));
            }
        }

        series.add(timeDiff, val);
        chartView.repaint();
        return orig;
    }

    private void updateChart1(double val) {
        if (niftyValueIndex == 0) {
            timeNiftyFirstVal = System.currentTimeMillis();
            chart1mRenderer.setYAxisMin(val - 10);
            chart1mRenderer.setYAxisMax(val + 10);

        } else {
            niftyDiffTime = System.currentTimeMillis() - timeNiftyFirstVal;
        }
        double min = chart1mRenderer.getYAxisMin();
        double max = chart1mRenderer.getYAxisMax();
        boolean flag = false;

        if (val > max) {
            chart1mRenderer.setYAxisMax(val);
            flag = true;
        } else if (val < min) {
            chart1mRenderer.setYAxisMin(val);
            flag = true;
        }
/*
        if(val>max || val<min){



            flag = true;
        }*/
        //chart1mRenderer.setYLabels(10);
        if (flag) {
            chart1chartView = ChartFactory.getLineChartView(this, chart1dataset, chart1mRenderer);
            chart1chartLyt.addView(chart1chartView, 0);
        }

        niftyDiffTime = niftyDiffTime / 1000;
        if (niftyDiffTime > 60) {
            niftyValues = new double[61000];
            niftyTimes = new long[61000];
            niftyValueIndex = 0;
            niftyDiffTime = 0;
            timeNiftyFirstVal = System.currentTimeMillis();
            chart1chartLyt.removeAllViews();
            chart1series.clear();
            chart1dataset.removeSeries(chart1series);
            chart1dataset.addSeries(chart1series);

            chart1mRenderer.setYAxisMin(val - 10);
            chart1mRenderer.setYAxisMax(val + 10);
            //chart1mRenderer.setYLabels(10);

            chart1chartView = ChartFactory.getLineChartView(this, chart1dataset, chart1mRenderer);

            chart1chartView.setOnClickListener(this);
            chart1chartLyt.addView(chart1chartView, 0);

        }
        min = chart1mRenderer.getYAxisMin();
        max = chart1mRenderer.getYAxisMax();

        if (flag) {
            for (int i = 0; i < niftyValueIndex; i++) {
                chart1series.add(niftyTimes[i], niftyValues[i]);
            }
        }


        //double rand = val + Math.random()*2;
        niftyValues[niftyValueIndex] = val;
        niftyTimes[niftyValueIndex] = niftyDiffTime;
        niftyValueIndex++;
        chart1series.add(niftyDiffTime, val);


        chart1chartView.repaint();
//        Log.d("min-max range", String.valueOf(min) + "-" + String.valueOf(max));
//        Log.d("nifty","nifty output "+val);
    }

    private void updateChart2(double val2) {
        if (sensaxValueIndex == 0) {
            timeSensaxFirstVal = System.currentTimeMillis();
            chart2mRenderer2.setYAxisMin(val2 - 10);
            chart2mRenderer2.setYAxisMax(val2 + 10);

        } else {
            sensaxDiffTime = System.currentTimeMillis() - timeSensaxFirstVal;
        }
        double min2 = chart2mRenderer2.getYAxisMin();
        double max2 = chart2mRenderer2.getYAxisMax();
        boolean flag2 = false;

        if (val2 > max2) {
            chart2mRenderer2.setYAxisMax(val2);
            flag2 = true;
        } else if (val2 < min2) {
            chart2mRenderer2.setYAxisMin(val2);
            flag2 = true;
        }
/*
        if(val>max || val<min){



            flag = true;
        }*/
        //chart1mRenderer.setYLabels(10);
        if (flag2) {
            chart2chartView2 = ChartFactory.getLineChartView(this, chart2dataset2, chart2mRenderer2);
            chart2chartLyt2.addView(chart2chartView2, 0);
        }

        sensaxDiffTime = sensaxDiffTime / 1000;
        if (sensaxDiffTime > 60) {
            sensaxValues = new double[61000];
            sensaxTimes = new long[61000];
            sensaxValueIndex = 0;
            sensaxDiffTime = 0;
            timeSensaxFirstVal = System.currentTimeMillis();
            chart2chartLyt2.removeAllViews();
            chart2series2.clear();
            chart2dataset2.removeSeries(chart2series2);
            chart2dataset2.addSeries(chart2series2);

            chart2mRenderer2.setYAxisMin(val2 - 10);
            chart2mRenderer2.setYAxisMax(val2 + 10);
            //chart1mRenderer.setYLabels(10);

            chart2chartView2 = ChartFactory.getLineChartView(this, chart2dataset2, chart2mRenderer2);
            chart2chartView2.setOnClickListener(this);
            chart2chartLyt2.addView(chart2chartView2, 0);


        }
        min2 = chart2mRenderer2.getYAxisMin();
        max2 = chart2mRenderer2.getYAxisMax();

        if (flag2) {
            for (int i = 0; i < sensaxValueIndex; i++) {
                chart2series2.add(sensaxTimes[i], sensaxValues[i]);
            }
        }


        //double rand = val + Math.random()*2;
        sensaxValues[niftyValueIndex] = val2;
        sensaxTimes[niftyValueIndex] = sensaxDiffTime;
        sensaxValueIndex++;
        chart2series2.add(sensaxDiffTime, val2);

        chart2chartView2.repaint();
//        Log.d("min-max range", String.valueOf(min2) + "-" + String.valueOf(max2));
//        Log.d("sensax","sensax output "+val2);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        socketClient.close();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void start(View v) {
        startButton.setClickable(false);
        stopButton.setClickable(true);
        startPlottingChart();
    }

    public void stop(View v) {
        if (niftyAnnotationClose != null) {
            niftyAnnotationClose.interrupt();
            if (chart1series.getAnnotationCount() != 0) {
                chart1series.removeAnnotation(0);
            }
        }
        niftyValueIndex = 0;

        if (sensaxAnnotationClose != null) {
            sensaxAnnotationClose.interrupt();
            if (chart2series2.getAnnotationCount() != 0) {
                chart2series2.removeAnnotation(0);
            }
        }
        sensaxValueIndex = 0;
        startButton.setClickable(true);
        stopButton.setClickable(false);

        chart1chartLyt.removeAllViews();
        chart2chartLyt2.removeAllViews();
    }

    @Override
    public void onClick(View v) {
        SeriesSelection seriesSelection = null;
        Thread thread = null;
        XYSeries series = null;
        XYMultipleSeriesRenderer mRenderer = null;
        GraphicalView chartView = null;


        if (((GraphicalView) v).equals(chart1chartView)) {
            seriesSelection = chart1chartView.getCurrentSeriesAndPoint();
            thread = niftyAnnotationClose;
            series = chart1series;
            mRenderer = chart1mRenderer;
            chartView = chart1chartView;

        } else if (((GraphicalView) v).equals(chart2chartView2)) {
            seriesSelection = chart2chartView2.getCurrentSeriesAndPoint();
            thread = sensaxAnnotationClose;
            series = chart2series2;
            mRenderer = chart2mRenderer2;
            chartView = chart2chartView2;

        }

        final XYMultipleSeriesRenderer finalMRenderer = mRenderer;

        // seriesSelection = chart1chartView.getCurrentSeriesAndPoint();
        // double[] xy = chart1chartView.toRealPoint(0);

        if (seriesSelection == null) {
            //Toast.makeText(getApplicationContext(), "No chart element was clicked", Toast.LENGTH_SHORT).show();
        } else {
            final double x = seriesSelection.getXValue();
            final double y = seriesSelection.getValue();
            final int index = seriesSelection.getPointIndex();
            // Toast.makeText(getApplicationContext(), "Chart element in chart1series index " + seriesSelection.getSeriesIndex() + " data point index " + index + " was clicked" + " closest point value X=" + x  + ", Y=" + y + " clicked point value X=" + (float) xy[0] + ", Y=" + (float) xy[1], Toast.LENGTH_SHORT).show();
            // seriesSelection.getPointIndex();
            // chart1series.getAnnotationAt(seriesSelection.getPointIndex());

            if (thread != null) {
                thread.interrupt();
                if (series.getAnnotationCount() != 0) {
                    series.removeAnnotation(0);
                    Double[] d = mRenderer.getXTextLabelLocations();
                    mRenderer.removeXTextLabel(d[0]);
                    d = mRenderer.getYTextLabelLocations();
                    mRenderer.removeYTextLabel(d[0]);
                }
            }

            series.addAnnotation("( " + x + ", " + y + " )", (int) x, y + .2);
            mRenderer.addXTextLabel(x, String.valueOf(x));
            mRenderer.addYTextLabel(y, String.valueOf(y));
            mRenderer.setShowCustomTextGrid(true);
            chartView.repaint();

            thread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        Thread.sleep(5000);
                        //clearAnnotation(index, x, y);
                        clearAnnotation(x, y, finalMRenderer);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();

            if (((GraphicalView) v).equals(chart1chartView)) {
                niftyAnnotationClose = thread;
            } else {
                sensaxAnnotationClose = thread;
            }
        }

//        {
//        SeriesSelection seriesSelection = chart1chartView.getCurrentSeriesAndPoint();
////        double[] xy = chart1chartView.toRealPoint(0);
//
//        if (seriesSelection == null) {
//            //Toast.makeText(getApplicationContext(), "No chart element was clicked", Toast.LENGTH_SHORT).show();
//        } else {
//            final double x = seriesSelection.getXValue();
//            final double y = seriesSelection.getValue();
//            final int index = seriesSelection.getPointIndex();
//            // Toast.makeText(getApplicationContext(), "Chart element in chart1series index " + seriesSelection.getSeriesIndex() + " data point index " + index + " was clicked" + " closest point value X=" + x  + ", Y=" + y + " clicked point value X=" + (float) xy[0] + ", Y=" + (float) xy[1], Toast.LENGTH_SHORT).show();
//            // seriesSelection.getPointIndex();
//            // chart1series.getAnnotationAt(seriesSelection.getPointIndex());
//
//            if (niftyAnnotationClose != null) {
//                niftyAnnotationClose.interrupt();
//                if (chart1series.getAnnotationCount() != 0) {
//                    chart1series.removeAnnotation(0);
//                    Double[] d = chart1mRenderer.getXTextLabelLocations();
//                    chart1mRenderer.removeXTextLabel(d[0]);
//                    d = chart1mRenderer.getYTextLabelLocations();
//                    chart1mRenderer.removeYTextLabel(d[0]);
//                }
//            }
//
//            chart1series.addAnnotation("( " + x + ", " + y + " )", (int) x, y + .2);
//            chart1mRenderer.addXTextLabel(x, String.valueOf(x));
//            chart1mRenderer.addYTextLabel(y, String.valueOf(y));
//            chart1mRenderer.setShowCustomTextGrid(true);
//            chart1chartView.repaint();
//
//            niftyAnnotationClose = new Thread() {
//                @Override
//                public void run() {
//                    super.run();
//                    try {
//                        Thread.sleep(5000);
//                        //clearAnnotation(index, x, y);
//                        clearAnnotation(x, y, chart1mRenderer);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            niftyAnnotationClose.start();
//        }
//    }
//
//    {
//        SeriesSelection seriesSelection2 = chart2chartView2.getCurrentSeriesAndPoint();
////            double[] xy2 = chart2chartView2.toRealPoint(0);
//
//        if (seriesSelection2 == null) {
//            //Toast.makeText(getApplicationContext(), "No chart element was clicked", Toast.LENGTH_SHORT).show();
//        } else {
//            final double x2 = seriesSelection2.getXValue();
//            final double y2 = seriesSelection2.getValue();
//            final int index2 = seriesSelection2.getPointIndex();
//            // Toast.makeText(getApplicationContext(), "Chart element in chart1series index " + seriesSelection.getSeriesIndex() + " data point index " + index + " was clicked" + " closest point value X=" + x  + ", Y=" + y + " clicked point value X=" + (float) xy[0] + ", Y=" + (float) xy[1], Toast.LENGTH_SHORT).show();
//            // seriesSelection.getPointIndex();
//            // chart1series.getAnnotationAt(seriesSelection.getPointIndex());
//
//            if (sensaxAnnotationClose != null) {
//                sensaxAnnotationClose.interrupt();
//                if (chart2series2.getAnnotationCount() != 0) {
//                    chart2series2.removeAnnotation(0);
//                    Double[] d2 = chart2mRenderer2.getXTextLabelLocations();
//                    chart2mRenderer2.removeXTextLabel(d2[0]);
//                    d2 = chart2mRenderer2.getYTextLabelLocations();
//                    chart2mRenderer2.removeYTextLabel(d2[0]);
//                }
//            }
//
//            chart2series2.addAnnotation("( " + x2 + ", " + y2 + " )", (int) x2, y2 + .2);
//            chart2mRenderer2.addXTextLabel(x2, String.valueOf(x2));
//            chart2mRenderer2.addYTextLabel(y2, String.valueOf(y2));
//            chart2mRenderer2.setShowCustomTextGrid(true);
//            chart2chartView2.repaint();
//
//            sensaxAnnotationClose = new Thread() {
//                @Override
//                public void run() {
//                    super.run();
//                    try {
//                        Thread.sleep(5000);
//                        //clearAnnotation2(index2, x2, y2);
//                        clearAnnotation(x2, y2, chart2mRenderer2);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            sensaxAnnotationClose.start();
//        }
//    }

    }

    private float getSP(float givenSize) {
        DisplayMetrics metrics = this.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, givenSize, metrics);
    }

}
