package com.daffodil.assignment2;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;

public class SocketManager {

    private static SocketManager instance = null;
    private String url = AppConst.URL;
//    private String arvindKeyMessage = AppConst.ARVIND_MESSAGE;
    private String otherPersonKeyMessage = AppConst.OTHER_MESSAGE;
    private WebSocketClient stockSocket2;

    public RecvMsgList recvMsgList=null;

    public static SocketManager getInstance() {
        if (instance == null) {
            instance = new SocketManager();
        }
        return instance;
    }

    public WebSocketClient getStockSocket() {
        connect();
        stockSocket2.connect();
        return stockSocket2;
    }

    private void connect() {
        URI uri = null;
        try {
            uri = new URI(url);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        stockSocket2 = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                stockSocket2.send(otherPersonKeyMessage);
            }

            @Override
            public void onMessage(String message) {
                if(recvMsgList!=null){
                    recvMsgList.onReceived(message);
                }
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {

            }

            @Override
            public void onError(Exception ex) {
                ex.printStackTrace();
            }
        };
    }

//    private Socket stockSocket1;
//    {
//        try {
//            stockSocket1 = IO.socket("ws://mtc.f6online.com:8000/ws");
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//    }
//    public Socket getStockSocket1() {
//        handler1 = new DataHandler1();
//        return stockSocket1;
//    }
//    public void init() {
//        stockSocket1.on("connect", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                stockSocket1.emit("arvindKeyMessage", "add;ARVIND-DEMO;-1;arvind");
//            }
//        }).on("disconnect", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                stockSocket1.disconnect();
//            }
//        }).on("global indices data", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Message message1 = new Message();
//                Bundle bundle = new Bundle();
//                bundle.putString("msg", args[0].toString());
//                message1.setData(bundle);
//                handler1.handleMessage(message1);
//            }
//        });
//
//        stockSocket1.connect();
//    }

}
