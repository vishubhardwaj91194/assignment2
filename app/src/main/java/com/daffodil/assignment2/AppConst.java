package com.daffodil.assignment2;

public class AppConst {
    public static int SENSEX_NIFTY_DATA_ID = 4;
    public static String JSON_KEY_ID = "1";
    public static String JSON_KEY_NAME = "2";
    public static String JSON_KEY_VALUE = "5";
    public static String JSON_KEY_FOUR = "4";
    public static String NIFTY_NAME = "NSE";
    public static String SENSAX_NAME = "BSE";

    public static String URL = "ws://mtc.f6online.com:8000/ws";
    public static String OTHER_MESSAGE = "add;9540389529-DEMO;-1;9540389529";
    public static String ARVIND_MESSAGE = "add;ARVIND-DEMO;-1;arvind";

    public static String NIFTY_LEGEND = "";
    public static float MEDIUM_TEXT_SIZE = 12;
    public static float SMALL_TEXT_SIZE = 10;

    public static boolean CHART1 = true;
    public static boolean CHART2 = false;


}
