package com.daffodil.assignment2;

public class RandomDataSend extends Thread {

    private RecvMsgList list;
    private double originalData;

    public RandomDataSend(RecvMsgList list) {
        this.list = list;
        originalData = Math.random()*100;

    }

    @Override
    public void run() {
        super.run();
        String message;
        double diff, temp;
        while (true) {
            diff = Math.random()*15;
            if(Math.floor(Math.random())==0){
                temp = originalData + diff;
            }else{
                temp = originalData - diff;
            }
            message = "{\"1\"=11,\"2\"=\"vishu\",\"4\"="+temp+",\"5\"="+originalData+"}";
            list.onReceived(message);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
