package com.daffodil.assignment2;

public interface RecvMsgList {
    void onReceived(String msg);
}
